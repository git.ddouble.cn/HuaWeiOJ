#include <iostream>
#include <string>
#include <string.h>
#include <memory>

using namespace std;

class Solution
{
	private:
		char * _str[2];
	public:
		Solution()
		{
			_str[0] = new char[1024];
			_str[1] = new char[1024];
		}
		~Solution()
		{
			delete _str[0];
			delete _str[1];
		}
		void set_str()
		{
			cin.getline(_str[0], 1024);
			cout<<strlen(_str[0])<<endl;
			cin.getline(_str[1], 1024);
			cout<<strlen(_str[1])<<endl;
		}
		int getMaxSubString()
		{
			int rst = 0;

			if(nullptr == _str[0] || nullptr == _str[1])
			{
				return -1;
			}
			if(nullptr != strstr(_str[0], _str[1]))
			{
				return strlen(_str[1]);
			}

			char * tmp_sub = nullptr;
			for(int i = 0, str1_len = (strlen(_str[1])/sizeof(char)); i < str1_len; ++i)
			{
				for(int j = i; j < str1_len; ++j)
				{
					tmp_sub = new char[j-i+1];
					for(int s = i, e = j, k = 0; s <=e; ++s, ++k)
					{
						tmp_sub[k] = _str[1][s];
					}

					int tmp_sub_len = j-i+1;
					if(nullptr != strstr(_str[0], tmp_sub))
					{
						if(rst <= tmp_sub_len)
						{
							rst = tmp_sub_len;
						}
						cout<<"sub:"<<tmp_sub<<endl;
					}
					else
					{
						delete tmp_sub;
						tmp_sub = nullptr;
						continue;
					}
					delete tmp_sub;
					tmp_sub = nullptr;
				}
			}
			if(nullptr != tmp_sub)
			{
				delete tmp_sub;
				tmp_sub = nullptr;
			}
			return rst;
		}
};

int main()
{
	Solution * test = new Solution();
	test->set_str();
	cout<<"rst:"<<test->getMaxSubString()<<endl;
	return 0;
}
