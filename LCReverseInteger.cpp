/*
Reverse digits of an integer.

Example1: x = 123, return 321
Example2: x = -123, return -321

click to show spoilers.

Note:
The input is assumed to be a 32-bit signed integer. Your function should return 0 when the reversed integer overflows.
*/
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

int reverse(int x)
{
    int rst = 0;
    while (x) {
        int tmp = rst * 10 + x % 10;
        if (tmp / 10 != rst) { //溢出判断
            return 0;
        }
        rst = tmp;
        x = x / 10;
    }
    return rst;
}

int main(int argc, const char * argv[])
{
    std::cout<<reverse(1114444444443);
    return 0;
}
