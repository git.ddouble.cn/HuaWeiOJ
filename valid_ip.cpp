#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <vector>
#include <set>
#include <array>
using  namespace std;


struct IP
{
    array<int,4> num;
    array<char,3> dot;
};
class Solution
{
public:
    static bool isValidIp(string str)
    {
        stringstream ss;
        ss<<str;
        IP ip;
        ss>>ip.num[0]>>ip.dot[0]>>ip.num[1]>>ip.dot[1]>>ip.num[2]>>ip.dot[2]>>ip.num[3];
        //bool rst = true;
        for(array<int,4>::iterator itr = ip.num.begin(); itr != ip.num.end(); ++itr)
        {
            if(!(0 <= *itr && *itr <= 255))
            {
                //rst = false;
                return false;
            }
        }
        if(46 == ip.dot[0] && 46 == ip.dot[1] && 46 == ip.dot[1])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};
int main()
{
    string str;
    cin>>str;
    Solution solution;
    if(solution.isValidIp(str))
    {
        cout<<"YES"<<endl;
    }
    else
    {
        cout<<"NO"<<endl;
    }
    return 0;
}
