#include <iostream>  
#include <string>  
#include <cstring>  
#include <vector>  
#include <sstream>  
#include <map>  
#include <iterator>  
#include <set>  
#include <array>  
#include <bitset>  
#include <algorithm>  
#include <locale>  
  /*
  题目

描述: 
实现删除字符串中出现次数最少的字符，若多个字符出现次数一样，则都删除。输出删除这些单词后的字符串，字符串中其它字符保持原来的顺序。

题目类别: 字符串 
难度: 中级 
运行时间限制: 10Sec 
内存限制: 128MByte 
阶段: 入职前练习 
输入: 
字符串只包含小写英文字母, 不考虑非法输入，输入的字符串长度小于等于20个字节。

输出: 
删除字符串中出现次数最少的字符后的字符串。

样例输入: 
abcdd

样例输出: 
dd
  
  */
  
  
std::array<int, 26> countChar(std::string & str)  
{  
    std::array<int, 26> countArr = {0};  
    for(std::string::iterator it = str.begin(); it != str.end(); ++it){  
        if(isupper(*it)){  
            *it = tolower(*it);  
        }  
        countArr[*it - 97]++;  
    }  
  
    return countArr;  
};  
  
std::vector<char> getDeleteList(std::array<int, 26> & arr)  
{  
    std::vector<char> rst;  
    int min = 100, minIdx;  
    //min_element  
    for(int i = 0; i != arr.size(); ++i){  
        if(min >= arr[i] && arr[i] >= 1){  
            min = arr[i];  
            minIdx = i;  
        }  
    }  
    //min'value_list  
    for(int i = 0; i != arr.size(); ++i){  
        if(arr[minIdx] == arr[i]){  
            rst.emplace_back(i+97);  
        }  
    }  
    return rst;  
}  
  
void print(std::string & str)  
{  
    std::array<int, 26> arr = countChar(str);  
    std::vector<char> deleteCharList = getDeleteList(arr);  
    std::string rst = "";  
    for(std::string::iterator it = str.begin(); it != str.end(); ++it){  
        bool f = false;  
        for(std::vector<char>::iterator jt = deleteCharList.begin(); jt != deleteCharList.end(); ++jt){  
            if(*it == *jt){  
                f = true;  
                break;  
            }  
        }  
        if(!f){  
            rst += *it;  
        }  
    }  
    std::cout<<rst<<std::endl;  
}  
int main()  
{  
    std::string t;// = "abcdD";  
    std::cin>>t;  
    print(t);  
    return 0;  
}  