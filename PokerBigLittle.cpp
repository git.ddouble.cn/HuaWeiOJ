#include <iostream>

#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
using namespace std;


map<string, int> poker;
void Init()
{
    string val[15] = { "3","4","5","6","7","8","9","10","J","Q","K","A","2","joker","JOKER" };
    for (int i = 0; i < 15; i++)
        poker[val[i]] = i+1;
}
vector<string> bigger(vector<string> &A, vector<string> &B)
{
    return poker[A[0]] > poker[B[0]] ? A : B;
}
bool isKing(vector<string> v)
{
    if (poker[v[0]] > 13 && poker[v[1]] > 13)
        return true;
    return false;
}
void PrintVector(vector<string> v)
{
    vector<string>::iterator iter = v.begin();
    for (; iter != v.end(); iter++) {
        if (iter != v.begin())
            cout << " ";
        cout << *iter;
    }
    cout << endl;
}
vector<string> split(const std::string& str, const std::string& seprator)
{
    vector<string> rst;
    std::string::size_type pos1, pos2;
    pos2 = str.find(seprator);
    pos1 = 0;
    while(std::string::npos != pos2)
    {
        rst.push_back(str.substr(pos1, pos2-pos1));

        pos1 = pos2 + seprator.size();
        pos2 = str.find(seprator, pos1);
    }
    if(pos1 != str.length())
        rst.push_back(str.substr(pos1));
    return rst;
}
int main()
{
    Init();
    vector<string> first, second;
    string input;

    getline(cin, input);
    string::size_type position = input.find('-');
    first = split(input.substr(0, position), " ");
    second = split(input.substr(position+1), " ");

    int len1 = first.size();
    int len2 = second.size();
    if (len1 == 2 || len2 == 2) {
        if (isKing(first)) {
            PrintVector(first);
            return 0;
        }
        if (isKing(second)) {
            PrintVector(second);
            return 0;
        }
    }
    if (len1 == 4 || len2 == 4) {
        if (len1 == 4 && len2 == 4)
            PrintVector(bigger(first, second));
        else
            PrintVector((len1 == 4) ? first : second);
    } else if (len1 == len2) {
        PrintVector(bigger(first, second));
    } else {
        cout << "ERROR" << endl;
    }
    return 0;

}
