#include <iostream>

#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <sstream>
#include <map>
/*
注意：he he不是命令。
为了简化输入，方便用户，以“最短唯一匹配原则”匹配：
1、若只输入一字串，则只匹配一个关键字的命令行。例如输入：r，根据该规则，匹配命令reset，执行结果为：reset what；输入：res，根据该规则，匹配命令reset，执行结果为：reset what；
2、若只输入一字串，但本条命令有两个关键字，则匹配失败。例如输入：reb，可以找到命令reboot backpalne，但是该命令有两个关键词，所有匹配失败，执行结果为：unkown command
3、若输入两字串，则先匹配第一关键字，如果有匹配但不唯一，继续匹配第二关键字，如果仍不唯一，匹配失败。例如输入：r b，找到匹配命令reset board，执行结果为：board fault。
4、若输入两字串，则先匹配第一关键字，如果有匹配但不唯一，继续匹配第二关键字，如果唯一，匹配成功。例如输入：b a，无法确定是命令board add还是backplane abort，匹配失败。
5、若输入两字串，第一关键字匹配成功，则匹配第二关键字，若无匹配，失败。例如输入：bo a，确定是命令board add，匹配成功。
6、若匹配失败，打印“unkonw command”
样例输入：

reset

reset board

board add

board delet

reboot backplane

backplane abort


样例输出：

reset what

board fault

where to add

no board at all

impossible

install first
 */
using namespace std;
class Solution{
private:
    map<string, string> relation;
public:
    Solution(){
        relation.insert(std::map<string,string>::value_type("reset", "reset what"));
        relation.insert(std::map<string,string>::value_type("reset board", "board fault"));
        relation.insert(std::map<string,string>::value_type("board add", "where to add"));
        relation.insert(std::map<string,string>::value_type("board delet", "no board at all"));
        relation.insert(std::map<string,string>::value_type("reboot backplane", "impossible"));
        relation.insert(std::map<string,string>::value_type("backplane abort", "install first"));
        relation.insert(std::map<string,string>::value_type("he he", "unkown command"));
    }

    void solution(string input){
        stringstream ss;
        ss<<input;
        string left, right;
        ss>>left>>right;

        if("" != left)
        {
            if("" != right)//两个字符串
            {
                std::map<string, string>::iterator it;
                stringstream first;
                string l, r, key;
                int flag = 0;
                for(it = relation.begin(); it != relation.end(); ++it)
                {
                    first<<(it->first);
                    first>>l>>r;
                    if(0 == l.find(left))
                    {
                        if(0 == r.find(right))
                        {
                            ++flag;
                            key = it->first;
                        }
                    }
                    l = r = "";
                    first.clear();
                }
                if(0 == flag || 1 < flag)
                {
                    cout<<relation.find("he he")->second<<endl;
                }
                else
                {
                    cout<<relation.find(key)->second<<endl;
                }
            }
            else//一个字符串
            {
                //(1)(2）
                std::map<string, string>::iterator it;
                stringstream first;
                string l, r;
                for(it = relation.begin(); it != relation.end(); ++it)
                {
                    first<<(it->first);
                    first>>l>>r;
                    if("" != r)//两个命令
                    {
                        l = r = "";
                        first.clear();
                        continue;
                    }
                    else//一个命令
                    {
                        if(0 == l.find(left))
                        {
                            cout<<relation.find(l)->second<<endl;
                            return;
                        }
                    }
                }
                cout<<relation.find("he he")->second<<endl;
            }
        }
    }
};

int main()
{
    Solution a;

    string temporary;
    while(getline(cin, temporary))
    {
        a.solution(temporary);
    }
    return 0;
}
/*      学习到的东西
 *
 *      1、stringstream的>>操作不能把“ ”输出到变量中，是以“ ”分割字符串，得到碎片
 *         stringstream如果想重复使用需要调用.clear（）方法，否则残留上次使用的记录
 *      2、map的使用方法：insert（map<..>::value_type({data})），find（key）返回一个迭代器iterator
 *      3、string的查找方法a.find(b):在a中查找b，返回第一次出现的位置
 *
 *
 * */