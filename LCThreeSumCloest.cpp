/*
Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

For example, given array S = {-1 2 1 -4}, and target = 1.

The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
    */
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>
#include <algorithm>


int threeSumClosest(std::vector<int>& nums, int target)
{
    std::sort(nums.begin(), nums.end());
    int delta = INT_MAX;
    int rst = 0;
    for (int i = 0; i < nums.size(); ++i) {
        int start = i + 1;
        int end = nums.size() - 1;
        while (start < end) {
            int sum = nums[i] + nums[start] + nums[end];
            if (std::abs(target - sum) < delta) {
                delta = std::abs(target - sum);
                rst = sum;
            }
            if (sum == target) {
                return sum;
            } else if (sum < target) {
                ++start;
            } else {
                --end;
            }
        }
    }
    return rst;
}


int main(int argc, const char * argv[])
{
    return 0;
}
