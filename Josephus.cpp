#include <iostream>
#include <vector>

using namespace std;

struct node{
	int id;
	int key;
};

class Solution2{
public:
      void function()
      {
          	vector<node> list;

	        int n, m, idx = 0;//

        	cout<<"input (n,m) begin...\n";
        	cin>>n>>m;
	        cout<<"input (n,m) end...\n\ninput node's key begin...\n";

    	    for(int i = 0; i < n; ++i)
	        {
		        int key;
		        cin>>key;
		        node item;
		        item.id = i;
		        item.key = key;			
		        list.push_back(item);
	        }
	        cout<<"input node's key end...\n\nresult is:\n";
	
	        while(list.size())
	        {
		         idx = (idx + m - 1) % list.size();
		         int _key = list[idx].key;
		         cout<<"[id,key] "<<list[idx].id<<" "<<list[idx].key<<endl; 			
		         list.erase(list.begin() + idx);		
		         m = _key;
	        }
	        cout<<"-----------------------------------------------";

	        char hold;
   	        cin>>hold;
	        while('a' != hold)
	        {
	   	        cin>>hold;	
	        }
 
      }
};
