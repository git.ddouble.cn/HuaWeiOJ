#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
#include <bitset>
#include <algorithm>
#include <locale>
/*
描写叙述: 
有这样一道智力题：“某商店规定：三个空汽水瓶能够换一瓶汽水。

小张手上有十个空汽水瓶，她最多能够换多少瓶汽水喝？”答案是5瓶，方法例如以下：先用9个空瓶子换3瓶汽水。喝掉3瓶满的，喝完以后4个空瓶子，用3个再换一瓶，喝掉这瓶满的，这时候剩2个空瓶子。

然后你让老板先借给你一瓶汽水，喝掉这瓶满的。喝完以后用3个空瓶子换一瓶满的还给老板。

假设小张手上有n个空汽水瓶。最多能够换多少瓶汽水喝？

题目类别: 循环 
难度: 中级 
执行时间限制: 10Sec 
内存限制: 128MByte 
阶段: 入职前练习 
输入: 
输入文件最多包括10组測试数据。每一个数据占一行，仅包括一个正整数n（1<=n<=100），表示小张手上的空汽水瓶数。n=0表示输入结束，你的程序不应当处理这一行。

输出: 
对于每组測试数据，输出一行。表示最多能够喝的汽水瓶数。假设一瓶也喝不到，输出0。

例子输入: 
3 
10 
81 
0

例子输出: 
1 
5 
40
*/

/*
        R(remainder) C(count) N(number)
        Ni = Ri + 3*Ci
          Ni' = Ni - 3*Ci
          Ni" = Ni' + Ci = Ri + Ci
        Ni+1 = Ri + Ci
*/
int pressurizedBottle(int & n)
{
    int r = 0, c = 0, sum = 0;

    while(n >= 2){
        r = n % 3;
        c = (n - r) / 3;
        sum += c;
        n = r + c;
        if(2 == n){
            sum++;
            break;
        }
    }
    return sum;
}
int main()
{
    int n;
    while(std::cin>>n){
        if(0 == n){
            break;
        }
        std::cout<<pressurizedBottle(n)<<std::endl;
    }
    return 0;
}