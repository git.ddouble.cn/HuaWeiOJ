#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
using namespace std;
/**
 描述
开发一个坐标计算工具， A表示向左移动，D表示向右移动，W表示向上移动，S表示向下移动。从（0,0）点开始移动，从输入字符串里面读取一些坐标，并将最终输入结果输出到输出文件里面。
输入：
合法坐标为A(或者D或者W或者S) + 数字（两位以内）
坐标之间以;分隔。
非法坐标点需要进行丢弃。如AA10;  A1A;  $%$;  YAD; 等。
下面是一个简单的例子 如：
A10;S20;W10;D30;X;A1A;B10A11;;A10;
处理过程：
起点（0,0）
+   A10   =  （-10,0）
+   S20   =  (-10,-20)
+   W10  =  (-10,-10)
+   D30  =  (20,-10)
+   x    =  无效
+   A1A   =  无效
+   B10A11   =  无效
+  一个空 不影响
+   A10  =  (10,-10）
结果 （10， -10）
知识点	字符串
运行时间限制	0M
内存限制	0
输入
一行字符串
输出
最终坐标，以,分隔
样例输入	A10;S20;W10;D30;X;A1A;B10A11;;A10;
样例输出	10,-10
 */

std::array<int, 2> location;

void init()
{
    location.at(0) = location.at(1) = 0;
}

vector<string> split(const std::string& str, const std::string& seprator)
{
    vector<string> rst;
    string::size_type pos1, pos2;
    pos2 = str.find(seprator);
    pos1 = 0;
    while(string::npos != pos2){
        rst.emplace_back(str.substr(pos1, pos2-pos1));
        pos1 = pos2 + seprator.size();
        pos2 = str.find(seprator,pos1);
    }
    if(seprator.length() >= pos1){
        rst.emplace_back(str.substr(pos1));
    }
    return rst;
}

void move(const char & direction, int num)
{
    if('A' == direction){
        location.at(0) -= num;
    }else if('D' == direction){
        location.at(0) += num;
    }else if('S' == direction){
        location.at(1) -= num;
    }else if('W' == direction){//
        location.at(1) += num;
    }
}

bool isNumber(string & str)
{
    for(string::iterator it = str.begin(); it != str.end(); ++it){
        if(*it > '9' || *it < '0'){
            return false;
        }
    }
    return true;
}

int main()
{
    string str = "A10;S20;W10;D30;X;A1A;B10A11;;A10;";
    str.clear();
    cin>>str;
    vector<string> rst = split(str, ";");
    for(vector<string>::iterator it = rst.begin(); it != rst.end(); ++it){
//        cout<<*it<<endl;
        if((*it).size() == 2){
            if((*it).at(0) == 'A' || (*it).at(0) == 'S' || (*it).at(0) == 'D' || (*it).at(0) == 'W'){
                string substr = (*it).substr(1);
                if(isNumber(substr)){
                    move((*it).at(0), (*it).at(1) - 48);
                }
            }
        }else if((*it).size() == 3){
            if((*it).at(0) == 'A' || (*it).at(0) == 'S' || (*it).at(0) == 'D' || (*it).at(0) == 'W'){
                string substr = (*it).substr(1);
                int num;
                stringstream ss;
                ss<<substr;
                ss>>num;
                if(isNumber(substr)){
                    move((*it).at(0), num);
                }
            }
        }else{
        }
    }
    cout<<location.at(0)<<','<<location.at(1)<<endl;
    return 0;
}
