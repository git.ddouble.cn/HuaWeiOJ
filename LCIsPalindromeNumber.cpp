/*

描述：Determine whether an integer is a palindrome. Do this without extra space.

Some hints:
Could negative integers be palindromes? (ie, -1)

If you are thinking of converting the integer to string, note the restriction of using extra space.

You could also try reversing an integer. However, if you have solved the problem "Reverse Integer", you know that the reversed integer might overflow. How would you handle such case?

There is a more generic way of solving this problem.

注意问题要求O(1)空间复杂度，注意负数不是回文数。

思路1：将输入整数转换成倒序的一个整数，再比较转换前后的两个数是否相等，但是这样需要额外的空间开销

思路2：每次提取头尾两个数，判断它们是否相等，判断后去掉头尾两个数。
*/
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>
//219ms
bool isPalindrome(int x)
{
    //negative number
    if (x < 0) {
        return false;
    }
    int len = 1;
    while (x / len >= 10) {
        len *= 10;
    }
    while (x > 0) {
        //get the head and tail number
        int left = x / len;
        int right = x % 10;
        if(left != right) {
            return false;
        } else {
            //remove the head and tail number
            x = (x % len) / 10;
            len /= 100;
        }
    }
    return true;
}

//232ms
bool isPalindrome(int x)
{
    if (x < 0){
        return false;
    }
    if (x < 10) {
        return true;
    }
    int reverseX = 0, tmpX = x;
    while (x) {
        reverseX = reverseX * 10 + x % 10;
        x /= 10;
    }
    bool rst = false;
    if ((reverseX + tmpX) == (2 * tmpX)) {
        rst = true;
    }
    return rst;
}

int main(int argc, const char * argv[])
{
    isPalindrome(11);
    return 0;
}
