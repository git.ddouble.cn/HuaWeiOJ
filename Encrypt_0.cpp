#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>

using  namespace std;

class Encrypt
{
public:
    static bool ifUpperCase(char a)
    {
        if(a >= 65 && a <= 90)
        { return true;}
        else
        { return false;}
    }
    static bool ifLowerCase(char a)
    {
        if(a >= 97 && a <= 122)
        { return true;}
        else
        { return false;}
    }
    static string encrypt(string data, string key)
    {
        //map[26] && flag[26]
        bool flag[26] = {false};
        string encrypt, map;
        transform(key.begin(),key.end(),key.begin(),::toupper);
        //Duplicate removal
        int j = 0;
        for(int i = 0, length = key.length(); i != length; ++i)
        {
            int c_idx = key.at(i) - 65;
            if(false == flag[c_idx])
            {
                flag[c_idx] = true;

                map += key.at(i);
            }
        }
        //complete
        string temp;
        for(int i = 0; i != 26; ++i)
        {
            if(false == flag[i])
            {
                temp = temp + (char)(i+65);
                continue;
            }
        }
        map = map+temp;
        //encrypt
        for(int i = 0, length = data.length(); i != length; ++i)
        {
            if(ifLowerCase(data[i]))
            {
                encrypt += ::tolower(map[data[i]-97]);
            }
            else if(ifUpperCase(data[i]))
            {
                encrypt += map[data[i]-65];
            }
            else
            {
                encrypt +=data[i];
            }
        }
        return encrypt;
    }
};

int main() {
    string a("Trailblazers");
    Encrypt b;
    cout<<b.encrypt("Attack AT DAWN",a)<<endl;
    return 0;
}

