#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>

using  namespace std;

class Encrypt
{
public:
    static bool ifUpperCase(char a)
    {
        if(a >= 65 && a <= 90)
        { return true;}
        else
        { return false;}
    }
    static bool ifLowerCase(char a)
    {
        if(a >= 97 && a <= 122)
        { return true;}
        else
        { return false;}
    }
    static bool ifNumeric(char a)
    {
        if(a >= 48 && a <= 57)
        {return true;}
        else
        {return false;}
    }
    static char char_trans_en(char a)
    {
       if(ifUpperCase(a))
       {
           return ::tolower((char)(((a-65+1)%26)+65));
       }
       else if(ifLowerCase(a))
       {
           return ::toupper((char)(((a-97+1)%26)+97));
       }
       else if(ifNumeric(a))
       {
           return (char)(48+((a-48)+1)%10);
       }
    }
    static char char_trans_de(char a)
    {
        if(ifUpperCase(a))
       {
           return ::tolower((char)(((a-65-1)%26)+65));
       }
       else if(ifLowerCase(a))
       {
           return ::toupper((char)(((a-97-1)%26)+97));
       }
       else if(ifNumeric(a))
       {
           return (char)(48+((a-48)-1)%10);
       }
    }
    static string encrypt(string data)
    {
        string encrypt;
        //exception
        if(data.size()>100)
        {
            return nullptr;
        }
        //encrypt
        for(int i = 0, length = data.length(); i != length; ++i)
        {
            encrypt += char_trans_en(data.at(i));
        }
        return encrypt;
    }
    static string decrypt(string encrypt)
    {
        string decrypt;
        //exception
        if(encrypt.size() > 100)
        {
            return nullptr;
        }
        //decrypt
        for(int i = 0, length = encrypt.length(); i != length; ++i)
        {
            decrypt += char_trans_de(encrypt.at(i));
        }
        return decrypt;
    }
};

int main()
{
    string a,b;
    cin>>a>>b;
    Encrypt test;
    cout<<test.encrypt(a)<<endl;
    cout<<test.decrypt(b)<<endl;
    return 0;
}
