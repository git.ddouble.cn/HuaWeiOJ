#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
#include <bitset>
#include <algorithm>
#include <locale>


/*
 描述:  密码要求:

1.长度超过8位

2.包括大小写字母.数字.其它符号,以上四种至少三种

3.不能有相同长度超2的子串重复

说明:长度超过2的子串


题目类别:  字符串,数组  
难度:  中级  
运行时间限制: 10Sec 
内存限制: 128MByte 
阶段:  入职前练习  
输入:  
一组或多组长度超过2的子符串。每组占一行

输出:  
如果符合要求输出：OK，否则输出NG

每行输出对应一组输入的结果；

样例输入: 021Abc9000
021Abc9Abc1
021ABC9000
021$bc9000

样例输出: OK
NG
NG
OK
 * */
using namespace std;

bool isLengthValid(const string & str)
{
    return str.length() > 8;
}

bool isKindsValid(string & str)
{
    bool num_f = false, low_c_f = false, up_c_f = false, other_f = false;
    for(string::iterator it = str.begin(); it != str.end(); ++it){
        if('0' <= *it && *it <= '9'){
            if(!num_f){
                num_f = true;
            }
        }else if('A' <= *it && *it <= 'Z'){
            if(!up_c_f){
                up_c_f = true;
            }
        }else if('a' <= *it && *it <= 'z'){
            if(!low_c_f){
                low_c_f = true;
            }
        }else{
            if(!other_f){
                other_f = true;
            }
        }
    }
    return (num_f + low_c_f + up_c_f + other_f) >= 3;
}

bool isSubstrValid(const string & pswd)
{
    string tmp = "";
    for(int i = 0, i_end = pswd.length() -3; i != i_end; i++){
        for(int j = i + 3; j != pswd.length(); ++j){
            tmp = pswd.substr(i, j-i);
            //...
            if(string::npos != (pswd.substr(j)).find(tmp)){
                return false;
            }
            //...
            tmp = "";
        }
    }
    return true;
}

int main()
{
    string tmp;// = "asD23";
    //cout<<isKindsValid(t)<<isLengthValid(t);
    vector<string> rst;
    while(getline(cin, tmp)){
        if(tmp.size() == 0){
            break;
        }
        if(isLengthValid(tmp) && isKindsValid(tmp) && isSubstrValid(tmp)){
            rst.emplace_back("OK");
        } else{
            rst.emplace_back("NG");
        }
    }
    for(vector<string>::iterator it = rst.begin(); it != rst.end(); ++it){
        cout<<*it<<endl;
    }
    return 0;
}