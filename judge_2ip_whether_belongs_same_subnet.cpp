#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <vector>

using  namespace std;

//To determine whether the two IP belong to the same subnet
struct IP
{
    int a, b, c, d;
    bool operator==(const IP & ip)
    {
        return a == ip.a && b == ip.b && c == ip.c && d == ip.d;
    }
};
class Solution
{
public:
    static bool judge(string str, IP & ip)
    {
        int pos = 0;
        ip.a = atoi(&str[pos]);
        if(ip.a > 255)
        {
            return false;
        }

        pos = str.find_first_of('.',pos);
        //pos++;
        ip.b = atoi(&str[pos+1]);
        if(ip.b > 255)
        {
            return  false;
        }

        pos = str.find_first_of('.',pos+1);
        ip.c = atoi(&str[pos+1]);
        if(ip.c > 255)
        {
            return false;
        }
        pos = str.find_first_of('.', pos+1);
        ip.d = atoi(&str[pos+1]);
        if(ip.d > 255)
        {
            return false;
        }
        return true;
    }
};
int main()
{
    string string1, string2, string3;
    while(cin>>string1>>string2>>string3)
    {
        Solution a;
        IP ip,ip1,ip2;
        if(a.judge(string1,ip) && a.judge(string2,ip1)&&a.judge(string3, ip2))
        {
            ip1.a = ip.a & ip1.a;
            ip1.b = ip.b & ip1.b;
            ip1.c = ip.c & ip1.c;
            ip1.d = ip.d & ip1.d;

            ip2.d = ip.d & ip2.d;
            ip2.c = ip.c & ip2.c;
            ip2.b = ip.b & ip2.b;
            ip2.a = ip.a & ip2.a;
            if(ip1 == ip2)
            {
                cout<<"0"<<endl;
            }
            else
            {
                cout<<"2"<<endl;
            }
        }
        else
        {
            cout<<"1"<<endl;
        }
    }
    return 0;
}
