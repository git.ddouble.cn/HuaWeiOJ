#include <iostream>

#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <sstream>
#include <map>
/*

描述
给定一个正整数N代表火车数量，0<N<10，接下来输入火车入站的序列，一共N辆火车，每辆火车以数字1-9编号。要求以字典序排序输出火车出站的序列号。
知识点	栈
运行时间限制	0M
内存限制	0
输入
有多组测试用例，每一组第一行输入一个正整数N（0<N<10），第二行包括N个正整数，范围为1到9。
输出
输出以字典序排序的火车出站序列号，每个编号以空格隔开，每个输出序列换行，具体见sample。
样例输入	3 1 2 3
样例输出	1 2 3
        1 3 2
        2 1 3
        2 3 1
        3 1 2
        3 2 1
 */
using namespace std;
class Solution{
public:
    Solution(){
    }

    void solution(vector<int> & vec)
    {
        vector<int> old(vec);
        int j, k;
        while(true){
            print(old,vec);
            for(int i = vec.size() - 1; 0 <= i; ){
                //todo j = max{i | Pi < Pi+1}
                if(0 == i){
                    return;
                }
                if(vec.at(i-1) <= vec.at(i)){
                    j = i-1;
                    for(int ii = vec.size() - 1; j != ii; )
                    {
                        //todo k = max{i | Pi > Pj}
                        if(vec.at(j) <= vec.at(ii)){
                            k = ii;
                            break;
                        }
                        else{
                            ii--;
                        }
                    }
                    //todo swap arr[j] arr[j]
                    swap(vec, j, k);
                    //todo reverse [j+1, n)
                    reverse(vec,j);
                    break;
                }
                else {
                   i--;
                }
            }

        }
    }

    void print(vector<int> & old, vector<int> & vec)
    {
        if(old.at(2) == vec.at(0)
                && old.at(1) == vec.at(2)
                && old.at(0) == vec.at(1)){
            return;
        }
        int i = 0;
        for(vector<int>::iterator it = vec.begin(); it != vec.end(); it++, i++){
            if(2 != i){
                cout<<*it<<" ";
            }else{
                cout<<*it<<endl;
            }
        }
    }

    void validCheck(vector<int> & vec)
    {

    }
    void swap(vector<int> & vec, int j, int k)    {

        int temp = vec.at(j);
        vec.at(j) = vec.at(k);
        vec.at(k) = temp;
    }
    //reverse [j+1, n)
    void reverse(vector<int> & vec, int j)
    {
        vector<int > tmp(vec);
        for(int i = j+1, k = vec.size()-1; i != vec.size(); ++i, k--)
        {
            vec.at(i) = tmp.at(k);
        }
    }
};

int main()
{
    Solution a;
    vector<int > arr;
    arr.reserve(3);//预先分派3个空间

    int num, temp;
    cin>>num;
    while(num > 0)
    {
        cin>>temp;
        arr.emplace_back(temp);//插入性能提升
        num--;
    }

    a.solution(arr);
    return 0;
}
/*      学习到的东西
 *      字典排序：
 设P是1～n的一个全排列:p=p1p2......pn=p1p2......pj-1pjpj+1......pk-1pkpk+1......pn
1）从排列的右端开始，找出第一个比右边数字小的数字的序号j（j从左端开始计算），即 j=max{i|pi<pi+1}
2）在pj的右边的数字中，找出所有比pj大的数中最小的数字pk，即 k=max{i|pi>pj}（右边的数从右至左是递增的，
     因此k是所有大于pj的数字中序号最大者）
3）对换pj，pk
4）再将pj+1......pk-1pkpk+1......pn倒转得到排列p'=p1p2.....pj-1pjpn.....pk+1pkpk-1.....pj+1，
   这就是排列p的下一个排列。

 * */