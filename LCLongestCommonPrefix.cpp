
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

std::string longestCommonPrefix(std::vector<std::string>& strs)
{
    if (strs.empty()) {
        return "";
    }
    long minSize = INT_MAX;
    for (int i = 0; i != strs.size(); ++i) {
        if ((strs[i]).empty()) {
            return "";
        }
        if (minSize < (strs[i]).size()) {
            minSize = (strs[i]).size();
        }
    }
    
    int j = 0, i = 0;;
    bool flag = true;
    while (flag && j != minSize) {
        i = 0;
        char preChar = (strs[i])[j];
        for (; i != strs.size();) {
            if ((strs.at(i)).at(j) == preChar) {
                i++;
            } else {
                flag = false;
                return (strs.at(0)).substr(0, j);
                break;
            }
        }
        j++;
    }
    return (strs.at(0)).substr(0, j);
}

int main(int argc, const char * argv[])
{
    std::vector<std::string> strs;
    strs.emplace_back("abcdef");
    strs.emplace_back("abcde");
//    strs.emplace_back("a");
//    strs.emplace_back("b");
//    strs.emplace_back("ab");
    std::cout<<longestCommonPrefix(strs)<<std::endl;
    return 0;
}
