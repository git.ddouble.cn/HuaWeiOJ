

/*
public static String intToRoman(int num) {
    String M[] = {"", "M", "MM", "MMM"};
    String C[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    String X[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    String I[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10];
}
*/

#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>


bool isMatch(std::string s, std::string p)
{    
    bool rst = false;
    return rst;
}

std::string intToRoman(int num)
{
    if (num <= 0 || num > 3999) {
        return "";
    }
    std::string table[4][9] = {
        {"I","II","III","IV","V","VI","VII","VIII","IX"},
        {"X","XX","XXX","XL","L","LX","LXX","LXXX","XC"},
        {"C","CC","CCC","CD","D","DC","DCC","DCCC","CM"},
        {"M","MM","MMM","","","","","",""}
    };
    std::string rst = "";
    int i = 0;
    while (num) {
        int tmp = num % 10;
        if (tmp == 0) {
            ++i;
            num = num / 10;
            continue;
        } else {
            rst = table[i][tmp - 1] + rst;
            ++i;
            num = num / 10;
        }
    }
    return rst;
}
int main(int argc, const char * argv[])
{
    intToRoman(166);
    return 0;
}
