#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
#include <bitset>
#include <algorithm>
#include <locale>

int function(const std::string & a, const std::string & b)
{
    int max = 0;
    std::string tmp = "";
    for(int i = 0; i != a.length(); ++i){
        for(int j = a.length(); j != i; --j){
            tmp = a.substr(i, j-i);
            int pos = b.find(tmp);
            if(std::string::npos == pos){
                continue;
            }else{
                int rst = j - i;
                if(max < rst){
                    max = rst;
                }
            }
            tmp = "";
        }
    }
    return max;
}

int main()
{
    std::string str1 = "", str2="";
    std::cin>>str1>>str2;
    std::transform(str1.begin(),str1.end(),str1.begin(),tolower);
    std::transform(str2.begin(),str2.end(),str2.begin(),tolower);
    if(str1.length() >= str2.length()){
        std::cout<<function(str2,str1);
    } else {
        std::cout << function(str1, str2);
    }
    return 0;
}