#include <string.h>
#include <iostream>
using namespace std;

int getLength(char* string, int length){
    if(NULL == string || length <= 0){
    	return 0;
    }
    int end = -1;
    int rst = 0;
    for(int i = length-1; i >= 0; i--){
	if(' ' == string[i])
	  continue;
	if(-1 == end)
	  end = i;
	if(' ' == string[i-1] || 0 == i){
	  rst = end-i+1;
 	  break;
	}
    }
    return rst;
}

int main(){
    char string[128];
    cin.getline(string, 128);
    cout<<getLength(string, strlen(string))<<endl;
}
