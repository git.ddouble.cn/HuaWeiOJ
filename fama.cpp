#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <vector>
#include <set>
using  namespace std;

/**
 *
 *    dp[j] = dp[j] + dp[j - weight[i] * k]
 *    j ∈ [0, total]
 *    weight[i] i ∈ [0, n)砝码重量遍历
 *    k ∈ [0, num[i]]砝码的个数
 *
 */
class Fama{
public:
    Fama()
    {}
    ~Fama()
    {}
    void Solution()
    {
        int n;
        cin>>n;
        int weight[n];
        int num[n];
        for(int i = 0; i != n; ++i)
        {
            cin>>weight[i];
        }
        for(int i = 0; i != n; ++i)
        {
            cin>>num[i];
        }
        int total = 0;
        for(int i = 0; i != n; ++i)
        {
            total += weight[i] * num[i];
        }
        int dp[total+1] = {0};
        for(int j = 0; j <= total; ++j)
        {
            for(int i = 0; i != n; ++i)
            {
                for(int k = 0; k <= num[i]; ++k)
                {
                    dp[j] = dp[j] | dp[j-weight[i]*k];
                }
            }
        }
        int rst = 0;
        for(int i = 0; i <= total; ++i)
        {
            if(dp[i])
            {
                ++rst;
            }
        }
        cout<<rst<<endl;
    }
};
int main()
{
            int n;
       int * weight = new int[n],
          * num = new int[n];
       cin>>n;
       for(int i = 0; i != n; ++i)
       {
          cin>>weight[i];
       }
       for(int i = 0; i != n; ++i)
       {
          cin>>num[i];
       }

        set<int> grp;
        grp.insert(0);
        for(int i = 0; i != n; ++i)
        {
            set<int> a_set = grp;

            for(int j = 0; j <= num[i]; ++j)
            {
               for(set<int >::iterator itr = a_set.begin(); itr != a_set.end(); ++itr)
               {
                   grp.insert(*itr + j * weight[i]);
               }
            }
        }
        cout<<grp.size();
   return 0;
}
