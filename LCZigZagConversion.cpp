
/*
The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"
Write the code that will take a string and make this conversion given a number of rows:

string convert(string text, int nRows);
convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
*/
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

std::string convert(std::string s, int numRows)
{
    if (numRows == 0 || s.size() == 0) {
        return s;
    }
    std::string arr[1024];
    //int , gap = numRows - 2;
    for (int i = 0; i <= s.size() - 1;) {
        for (int j = 0; i <= s.size()-1 && j <= numRows-1; j++) {
            arr[j] += s[i];
            i++;
        }
        for (int j = numRows - 2; i <= s.size()-1 && j >= 1; j--) {
            arr[j] += s[i];
            i++;
        }
    }
    std::string rst;
    
    for (int i = 0 ; i <= numRows - 1; ++i) {
        rst += arr[i];
    }
    return rst;
}
int main(int argc, const char * argv[])
{
    std::cout<<convert("ABC", 3);
    return 0;
}
