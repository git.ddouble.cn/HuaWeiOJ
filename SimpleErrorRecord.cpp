#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
#include <bitset>
#include <algorithm>
/*
 题目

描述:

开发一个简单错误记录功能小模块，能够记录出错的代码所在的文件名称和行号。

处理： 

1、 记录最多8条错误记录，循环记录，对相同的错误记录（净文件名称和行号完全匹配）只记录一条，错误计数增加；

2、 超过16个字符的文件名称，只记录文件的最后有效16个字符；

3、 输入的文件可能带路径，记录文件名称不能带路径。


入职前练习  
输入:

一行或多行字符串。每行包括带路径文件名称，行号，以空格隔开。
如：E:\V1R2\product\fpgadrive.c   1325
输出:

将所有的记录统计并将结果输出，格式：文件名 代码行数 数目，一个空格隔开，如：
fpgadrive.c 1325 1
样例输入:

E:\V1R2\product\fpgadrive.c   1325
样例输出:

fpgadrive.c 1325 1

 
 ***************************
 *     循环记录，最多八条
 * ************************************
 * */
class Error
{
private:
    std::string name;
    int line;
    int count;
public:
    Error(std::string _name, int _line, int _count)
            :name(_name),line(_line),count(_count)
    {
    }
    ~Error()
    {
    }
    bool operator==(const Error & rhs)
    {
        return this->name == rhs.getName() && this->line == rhs.getLine();
    }
    void setName(std::string _name){this->name = _name;}
    void setLine(int _line){this->line = _line;}
    void setCount(int _count){this->count = _count;}
    std::string getName()const {return this->name;}
    int getLine()const { return  this->line;}
    int getCount()const { return this->count;}
};

std::string parse(std::string str)
{
    int pos = str.find_last_of("\\");
    std::string tmp;
    if(std::string::npos != pos){
        tmp = str.substr(pos+1);
    }else{
        tmp = str;
    }
    if(tmp.size() > 16){
        tmp = tmp.substr(tmp.size()-16);
    }
    return tmp;
}

int main()
{
    std::vector<Error> error;
    error.reserve(8);
    std::string pathname;
    int line;
    std::stringstream ss;
    ss.clear();
    while(std::getline(std::cin,pathname)){
        if(pathname.length() == 0){
            break;
        }
        ss<<pathname;
        ss>>pathname>>line;
        ss.clear();
        Error tmp(parse(pathname), line, 1);
        std::vector<Error>::iterator it = error.begin();
        for(; it != error.end(); ++it){
            if(*it == tmp){
                break;
            }
        }
        if(error.end() == it){
            error.emplace_back(tmp);
            if(error.size() == 9){
                error.erase(error.begin());
            }
        }else{
            (*it).setCount((*it).getCount()+1);
        }
    }
    for(std::vector<Error>::iterator it = error.begin(); it != error.end(); ++it){
        std::cout<<(*it).getName()<<" "<<(*it).getLine()<<" "<<(*it).getCount()<<std::endl;
    }
    return 0;
}