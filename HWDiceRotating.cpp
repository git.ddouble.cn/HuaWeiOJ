#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <iterator>
#include <set>
#include <array>
#include <bitset>
#include <algorithm>
#include <locale>
#include <climits>

/*
    华为2018暑期实习题目
    色子旋转
    色子的六个面，初始化为123456,左1右2前3后4上5下6,
    左旋转L,右旋转R，前旋转F，后旋转B，上旋转A，下旋转C
    示例：
    输入：RA
    输出：436512
*/
using namespace std;

void process(string & box, char ch)
{
    char tmp1, tmp2;
    if (ch == 'A') {
        tmp1 = box[2];
        tmp2 = box[3];
        box[2] = box[0];
        box[3] = box[1];
        box[0] = tmp2;
        box[1] = tmp1;
        return;
    }
    if (ch == 'C') {
        tmp1 = box[0];
        tmp2 = box[1];
        box[0] = box[2];
        box[1] = box[3];
        box[2] = tmp2;
        box[3] = tmp1;
        return;
    }
    if (ch == 'L') {
        tmp1 = box[0];
        tmp2 = box[1];
        box[0] = box[4];
        box[1] = box[5];
        box[4] = tmp2;
        box[5] = tmp1;
        return;
    }
    if (ch == 'R') {
        tmp1 = box[4];
        tmp2 = box[5];
        box[4] = box[0];
        box[5] = box[1];
        box[0] = tmp2;
        box[1] = tmp1;
        return;
    }
    if (ch == 'F') {
        tmp1 = box[2];
        tmp2 = box[3];
        box[2] = box[4];
        box[3] = box[5];
        box[4] = tmp2;
        box[5] = tmp1;
        return;
    }
    if (ch == 'B') {
        tmp1 = box[4];
        tmp2 = box[5];
        box[4] = box[2];
        box[5] = box[3];
        box[2] = tmp2;
        box[3] = tmp1;
        return;
    }
}
int main(){
    string instruct;
    string box = "123456";
    while (cin>>instruct){
        for (int i = 0; i < instruct.length(); ++i) {
            process(box, instruct[i]);
            //cout<<box<<endl;
        }
        cout<<box<<endl;
    }
    return 0;
}