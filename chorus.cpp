/*
	华为oj：合唱队
	动态规划：
	问题描述：给定一组序列如{8,186,186,150,200,160,130,197,200}
	解题方案：求longest increasing subsequence = len0
			  求longest descending subsequence = len1
			  max(len0 + len1)
 */

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <iomanip>
using namespace std;

class Solution
{
	public:
		Solution()
		{}
		~Solution()
		{}		
		void func(int arr[], int size)
		{
			int * increase = new int[size];
			int * descend = new int[size];

			for(int i = 0; i != size; ++i)
			{
				increase[i] = 1;
				for(int j = 0; j != i; ++j)
				{
					if((arr[i] > arr[j]) && (increase[j] + 1 > increase[i]))
					{
						increase[i] = increase[j]+1; 
					}
				}
			}
			for(int i = size - 1; i != -1; i--)
			{
				descend[i] = 1;
				for(int j = size - 1; j != i; j--)
				{
					if((arr[i] > arr[j]) && (descend[j] + 1 > descend[i]))
					{
						descend[i] = descend[j] + 1;
					}
				}
			}
	
			int rst0 = 0;
			for(int i = 0; i != size; ++i)
			{
				if(increase[i] + descend[i] > rst0)
				{
					rst0 = increase[i] + descend[i] - 1;
				}
			}
			cout<<(size - rst0)<<endl;
		}
};

int main()
{
//	int array[] = {8, 186, 186,	150, 200, 160, 130, 197, 200};
//	cout<<"source data:"<<endl;
//	for(size_t e : array)
//	{
//		cout<<e<<" ";
//	}
//	cout<<endl<<"new data:"<<endl;

	int arrary[225];
	int n;
	cin>>n;
	for(int i = 0; i < n; ++i)
	{
		cin>>arrary[i];
	}
	Solution a;
	a.func(arrary, n);
	return 0;
}
