
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

int romanToInt(std::string s)
{
    char table[7] = {'M','D','C','L','X','V','I'};
    int value[7] = {1000,500,100,50,10,5,1};
    std::map<char, int> m;
    for (int i = 0; i <= 6; i++) {
        m[table[i]] = value[i];
    }
    int rst = 0;
    for (int i = 0; i <= s.length() - 1; ++i) {
        int tmp = m[s[i]];
        bool f = false;
        for (int j = i + 1; j <= s.length() - 1; ++j) {
            if (m[s[j]] > tmp){
                f = true;
                break;
            }
        }
        if (f) {
            tmp *= -1;
        }
        rst += tmp;
    }
    return rst;
}

int main(int argc, const char * argv[])
{
    std::cout<<romanToInt("MCCDXLV")<<std::endl;
    return 0;
}
