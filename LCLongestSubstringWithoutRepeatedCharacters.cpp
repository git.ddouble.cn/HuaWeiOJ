/*
Given a string, find the length of the longest substring without repeating characters.

Examples:

Given "abcabcbb", the answer is "abc", which the length is 3.

Given "bbbbb", the answer is "b", with the length of 1.

Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a

substring, "pwke" is a subsequence and not a substring.
*/


#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

int lengthOfLongestSubstring(std::string str)
{
    if (str.length() <= 1) {
        return (int)str.length();
    }
    int rst = 0;
    std::map<char, int> _map;
    _map.clear();
    for(int i = 0, j = 1; i != str.length() && j != str.length();) {
        _map.insert(std::map<char, int>::value_type(str[i], i));
        
        while (j < str.length()) {
            std::map<char, int>::iterator itr = _map.find(str[j]);
            if (itr == _map.end()) {
                _map.insert(std::map<char, int>::value_type(str[j], j));
                ++j;
            } else {
                if (rst < _map.size()) {
                    rst = (int)_map.size();
                }
                i = itr->second+1;
                j = i+1;
                _map.clear();
                _map.insert(std::map<char, int>::value_type(str[i], i));
            }
        }
        if(rst < _map.size()) {
            rst = (int)_map.size();
        }
        _map.clear();
    }
    return rst;
}

int main(int argc, const char * argv[])
{
    lengthOfLongestSubstring("pw");
    return 0;
}
