#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <sstream>
#include <vector>
using  namespace std;


int Solution(int v[],int p[],int q[],int n,int m)
{
    int d[60][32000],i,j;
    bool flag[60][32000];
    memset(d,0,sizeof(d));
    memset(flag,false,sizeof(flag));
    for(i=1;i<=m;i++)
    {
        for(j=0;j<=n;j++)
        {
            if(q[i]==0)
            {
                d[i][j]=d[i-1][j];
                if(v[i]<=j&&d[i][j]<d[i-1][j-v[i]]+v[i]*p[i])
                {
                    d[i][j]=d[i-1][j-v[i]]+v[i]*p[i];
                    flag[i][j]=true;
                }
            }
            else
            {
                d[i][j]=d[i-1][j];
                if(v[i]<=j&&d[i][j]<d[i-1][j-v[i]]+v[i]*p[i])
                {
                    if(flag[q[i]][j-v[i]])
                        d[i][j]=d[i-1][j-v[i]]+v[i]*p[i];
                }
            }
        }
    }
    return d[m][n];
}

int main()
{
    int v[60],p[32000],q[60],n,m;
    cin>>n>>m;
    for(int i=1;i<=m;i++)
    {
        cin>>v[i]>>p[i]>>q[i];
    }
    cout<<Solution(v,p,q,n,m)<<endl;
    return 0;
}




