#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <set>
#include <climits>
#include <map>
/*

小Q今天在上厕所时想到了这个问题：有n个数，两两组成二元组，差最小的有多少对呢？差最大呢？


输入描述:

 输入包含多组测试数据。

 对于每组测试数据：

 N - 本组测试数据有n个数

 a1,a2...an - 需要计算的数据

 保证:

 1<=N<=100000,0<=ai<=INT_MAX.



输出描述:

对于每组数据，输出两个数，第一个数表示差最小的对数，第二个数表示差最大的对数。

输入例子:
6
45 12 45 32 5 6

输出例子:
1 2
 * */

int main()
{
    int n;
    while(std::cin>>n){
        if(n < 0){
            break;
        }
        std::map<int,int> m;
        int tmp;
        bool flag = false;
        for(int i = 0; i != n; ++i){
            std::cin>>tmp;
            std::map<int, int>::iterator iter = m.find(tmp);
            if(iter != m.end()){
                m[tmp]++;
                flag = true;
            }else{
                m.insert(std::map<int,int>::value_type(tmp,1));
            }
        }

        int rst0 = 0, rst1 = 0;

        if(flag){
            for(std::map<int, int>::iterator it = m.begin(); it != m.end(); ++it){
                if((*it).second > 1){
                    rst0 += (*it).second * ((*it).second - 1) / 2;
                }
            }
        }else{
            std::map<int, int>::iterator itPre = m.begin() ;
            int minValue = -1 ;
            for ( std::map<int, int>::iterator itCurr = ( ++ m.begin() ); itCurr != m.end(); ++ itCurr, ++ itPre ) {
                int dvalue = ( *itCurr ).first - ( *itPre ).first ;
                if ( minValue == -1 || dvalue < minValue ) {
                    rst0 = ( *itCurr ).second * ( *itPre ).second ;
                    minValue = dvalue ;
                }
                else if ( dvalue == minValue ) {
                    rst0 += ( *itCurr ).second * ( *itPre ).second ;
                }
            }
        }
        rst1 = (*m.begin()).second * (*m.rbegin()).second;
        std::cout<<rst0<<" "<<rst1<<std::endl;
    }

    return 0;
}
