#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>

using  namespace std;


int main() {

    int range;
    cin>>range;
    if(range < 1 || range > 30000)
    {
        return 0;
    }
    int count = 0;
    for(int i = 1; i <= range; ++i)
    {
        if(0 == i % 7)
        {
            ++count;
        }
        else
        {
            string temp = to_string(i);
            int pos = temp.find('7');
            int length = temp.length();
            if(0 <= pos && pos != length)
            {
                ++count;
            }
        }
    }
    cout<<count<<endl;
    return 0;

}

