
#include <iostream>
#include <map>
#include <string>
#include <deque>
#include <stack>
#include <vector>
#include <set>

/*
 *       1: "   12345"
 *       2: "   b1234","++12345","+-1234"
 *       3: "   123a234","  123 456"
 *       4: overflow
 */
int myAtoi(std::string str)
{
    int i = 0;
    for (; i <= str.size() - 1;) {
        if (str[i] == ' ') {
            ++i;
        } else {
            break;
        }
    }
    str = str.substr(i);
    
    //处理符号
    int symbol = 1;//正数
    if (str[0] == '+') {
        symbol = 1;
        str = str.substr(1);
    } else if (str[0] == '-') {
        symbol = -1;
        str = str.substr(1);
    } else if (str[i] >= '0' && str[i] <= '9') {
        
    }
    //处理数字和其他字符
    i = 0;
    int curr = 0;
    for (; i <= str.size() - 1; ++i) {
        if (str[i] >= '0' && str[i] <= '9') {
            int tmp = curr * 10 + (str[i] - '0');
            if (tmp / 10 != curr) {
                if (symbol == 1) {
                    return INT_MAX;
                } else {
                    return INT_MIN;
                }
            }
            curr = tmp;
        } else {
            break;
        }
    }
    return curr * symbol;
}

int main(int argc, const char * argv[])
{
    std::cout<<myAtoi("21555555555");
    return 0;
}